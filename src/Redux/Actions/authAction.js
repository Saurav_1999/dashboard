export const login = (num) => {
    return{
        type: 'LOG_IN',
        payload: num
    };
};
 
export const decrement = () => {
    return{
        type: 'LOG_OUT',
    };
};