import React from 'react'

const UserTable = () => {
    const data = [
        {
            id: '1',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '2',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '3',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '4',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '5',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '6',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '7',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '8',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '9',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        },
        {
            id: '10',
            firstName: 'Saurav',
            lastName: 'Dahal',
            pMode: 'UPI',
            PStatus: 'success',
            mobileNumber: '8967452319',
            slotBooked: '12/03/2022 , 8:35am To 09:05am',
            saloonName: 'Unique Saloon',
            barburName: 'pop'
        }
        // {
        //     id: '11',
        //     firstName: 'Saurav',
        //     lastName: 'Dahal',
        //     pMode: 'UPI',
        //     PStatus: 'success',
        //     mobileNumber: '8967452319',
        //     slotBooked: '12/03/2022 , 8:35am To 09:05am',
        //     saloonName: 'Unique Saloon',
        //     barburName: 'pop'
        // },
        // {
        //     id: '12',
        //     firstName: 'Saurav',
        //     lastName: 'Dahal',
        //     pMode: 'UPI',
        //     PStatus: 'success',
        //     mobileNumber: '8967452319',
        //     slotBooked: '12/03/2022 , 8:35am To 09:05am',
        //     saloonName: 'Unique Saloon',
        //     barburName: 'pop'
        // }
    ]
    return (
        <div>
            <div className='w-70 shadow-lg'>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">paymentMode</th>
                            <th scope="col">paymentStatus</th>
                            <th scope="col">contactNumber</th>
                            <th scope="col">sloteBooked</th>
                            <th scope="col">saloonName</th>
                            <th scope="col">barburName</th>
                        </tr>
                    </thead>
                </table>
            </div>
            {data.map((user) => (
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">{user.id}</th>
                            <td>{user.firstName}</td>
                            <td>{user.lastName}</td>
                            <td>{user.pMode}</td>
                            <td>{user.PStatus}</td>
                            <td>{user.mobileNumber}</td>
                            <td>{user.slotBooked}</td>
                            <td>{user.saloonName}</td>
                            <td>{user.barburName}</td>
                        </tr>
                    </tbody>
                </table>
            ))}
        </div>
    )
}
export default UserTable;