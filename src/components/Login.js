import React from 'react'
import { useState ,useHistory } from 'react'
import { useNavigate } from "react-router-dom";

const Login = () => {
    const [phone , setPhone] = useState("")
    let navigate = useNavigate();
    const login = async() => {
        const response = await fetch('http://localhost:5000/auth/phone' , {
            method : "post",
            headers : {
                "Content-Type" : "application/json",
            },
            body : JSON.stringify({phone})
        })

        const res = await response.json();
        if(res.success){
            console.log(res.data);
            localStorage.setItem('authToken', res.token);
            navigate("/dashboard");   
        }else{
            alert(res.msg)
        }
    }
  return (
   <>
    <div className='container py-5 bg-success w-50 align-self-center'>
        <input type='text' className='w-100' value = {phone} onChange = {(e) =>{
            setPhone(e.target.value)
            if(phone.length > 10){
                alert("lfghalkdfj")
            }
            }} placeholder='enter your contact number' required/>
        <button className='btn btn-primary w-50 btn-md center' value='submit' onClick={login}>submit</button>
    </div>

   </>
  )
}

export default Login


// import React from 'react';
// import { Formik } from 'formik';

// const Basic = () => (
//   <div>
//     <h1>Anywhere in your app!</h1>
//     <Formik
//       initialValues={{ email: '', password: '' }}
//       validate={values => {
//         const errors = {};
//         if (!values.email) {
//           errors.email = 'Required';
//         } else if (
//           !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
//         ) {
//           errors.email = 'Invalid email address';
//         }
//         return errors;
//       }}
//       onSubmit={(values, { setSubmitting }) => {
//         setTimeout(() => {
//           alert(JSON.stringify(values, null, 2));
//           setSubmitting(false);
//         }, 400);
//       }}
//     >
//       {({
//         values,
//         errors,
//         touched,
//         handleChange,
//         handleBlur,
//         handleSubmit,
//         isSubmitting,
//         /* and other goodies */
//       }) => (
//         <form onSubmit={handleSubmit}>
//           <input
//             type="email"
//             name="email"
//             onChange={handleChange}
//             onBlur={handleBlur}
//             value={values.email}
//           />
//           {errors.email && touched.email && errors.email}
//           <input
//             type="password"
//             name="password"
//             onChange={handleChange}
//             onBlur={handleBlur}
//             value={values.password}
//           />
//           {errors.password && touched.password && errors.password}
//           <button type="submit" disabled={isSubmitting}>
//             Submit
//           </button>
//         </form>
//       )}
//     </Formik>
//   </div>
// );

// export default Basic;