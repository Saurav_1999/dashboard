import React from 'react'
import Pagination from 'react-bootstrap/Pagination';
// import PageItem from 'react-bootstrap/PageItem';

let active = 1;
let items = [];
for (let number = 1; number <= 10; number++) {
items.push(
<Pagination.Item key={number} active={number === active}>
  {number}
</Pagination.Item>,
);
}
const PaginationUser = () => {

  return (
    <div>
      <div className='w-100 justify-center'>
      <Pagination className='bg-dark'>{items}</Pagination>
      </div>
    </div>
  )
}

export default PaginationUser;