import React from 'react';
import { useEffect , useState } from 'react'
import Footer from './Footer';
import Header from './Header';
import PaginationUser from './PaginationUser';
import Sidebar from './Sidebar';
import UserTable from './userTable';

const Dashboard = () => {
  const [total , setTotal] = useState(0);
  useEffect(async() => {
    const response = await fetch('http://localhost:5000/profile' , {
            method : "get",
            headers : {
                "Content-Type" : "application/json",
                "authorization" : "Bearer "+localStorage.getItem('authToken')
            }
        })

        const res = await response.json();
        if(res.success){
            setTotal(res.data.total)
        }else{
            alert(res.msg)
        }
  })
  return (
    <div>{/* Content Wrapper. Contains page content */}
<Header/>
<div className="content-wrapper">
  {/* Content Header (Page header) */}
  <div className="content-header">
    <div className="container-fluid">
      <div className="row mb-2">
        <div className="col-sm-6">
          <h1 className="m-0">Dashboard</h1>
        </div>{/* /.col */}
        <div className="col-sm-6">
          <ol className="breadcrumb float-sm-right">
            
            <li className="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>{/* /.col */}
      </div>{/* /.row */}
    </div>{/* /.container-fluid */}
  </div>
  {/* /.content-header */}
  {/* Main content */}
  <section className="content">
    <div className="container-fluid">
      {/* Small boxes (Stat box) */}
      <div className="row justify-content-around">
        <div className="col-lg-2 col-6">
          {/* small box */}
          <div className="small-box bg-info">
            <div className="inner">
              <h3>{total}</h3>
              <p>Today's Orders</p>
            </div>
            <div className="icon">
              <i className="ion ion-bag" />
            </div>
            <a href="#" className="small-box-footer">More info <i className="fas fa-arrow-circle-right" /></a>
          </div>
        </div>
        {/* ./col */}
         {/* ./col */}
         <div className="col-lg-2 col-6">
          {/* small box */}
          <div className="small-box bg-success">
            <div className="inner">
              <h3>53<sup style={{fontSize: 20}}>%</sup></h3>
              <p>Completed Order</p>
            </div>
            <div className="icon">
              <i className="ion ion-stats-bars" />
            </div>
            <a href="#" className="small-box-footer">More info <i className="fas fa-arrow-circle-right" /></a>
          </div>
        </div>
        {/* ./col */}
         {/* ./col */}
        {/* ./col */}
        <div className="col-lg-2 col-6">
          {/* small box */}
          <div className="small-box bg-success">
            <div className="inner">
              <h3>53<sup style={{fontSize: 20}}>%</sup></h3>
              <p>Today's COD </p>
            </div>
            <div className="icon">
              <i className="ion ion-stats-bars" />
            </div>
            <a href="#" className="small-box-footer">More info <i className="fas fa-arrow-circle-right" /></a>
          </div>
        </div>
        {/* ./col */}
        <div className="col-lg-2 col-6">
          {/* small box */}
          <div className="small-box bg-secondary">
            <div className="inner">
              <h3 className='text-white'>44</h3>
              <p className='text-white'>Online Transactctions</p>
            </div>
            <div className="icon">
              <i className="ion ion-person-add" />
            </div>
            <a href="#" className="small-box-footer">More info <i className="fas fa-arrow-circle-right" /></a>
          </div>
        </div>
        {/* ./col */}
        <div className="col-lg-2 col-6">
          {/* small box */}
          <div className="small-box bg-danger">
            <div className="inner">
              <h3>65</h3>
              <p>Canceld</p>
            </div>
            <div className="icon">
              <i className="ion ion-pie-graph" />
            </div>
            <a href="#" className="small-box-footer">More info <i className="fas fa-arrow-circle-right" /></a>
          </div>
        </div>
        {/* ./col */}
      </div>
      {/* /.row (main row) */}
    </div>{/* /.container-fluid */}
  </section>
  {/* /.content */}
{/* /.content-wrapper */}

{/* Table */}
<UserTable/>
<Sidebar/>
<PaginationUser/>
<Footer/>
</div>
</div>
  )
}

export default Dashboard